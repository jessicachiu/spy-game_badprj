import dotenv from "dotenv";
import { logger } from "./helpers/logger-Util";
dotenv.config();
 
const env = {
    DB_NAME: process.env.DB_NAME,
    DB_USERNAME: process.env.DB_USERNAME,
    DB_PASSWORD: process.env.DB_PASSWORD,
    PORT: process.env.PORT,
    ai_host: process.env.ai_host || "http://192.168.0.101:8108",
    LOG_LEVEL : process.env.LOG_LEVEL || 'debug'
    // ai_host: process.env.ai_host || "http://192.168.59.64:8108",

    // ai_host: "http://localhost:8108"

    // GOOGLE_CLIENT_ID:process.env.GOOGLE_CLIENT_ID,
    // GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
 
 
};

export function checkEnv(){
    // let logger = getLogger()
    // console.log('logger = ', logger);
    
    logger.warn(`Log level : ${env.LOG_LEVEL}`);
    for (let envKey in env){
        let envVal = env[envKey]
    
        if (!envVal){
            // console.log(`WARNING ! Cannot find ${envKey} in .env`)
            logger.warn(`WARNING ! Cannot find ${envKey} in .env`);
        }
    }

}

 
export default env;