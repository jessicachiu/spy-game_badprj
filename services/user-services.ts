import { User } from "../models/UserModels";
import { hashPassword } from "../helpers/hash"
import { Client } from "pg";

// import Knex from 'knex';

// const knexConfigs = require('../knexfile');

// const configMode = process.env.NODE_ENV || "development";
// const knexConfig = knexConfigs[configMode];

// //@ts-ignore
// // const knex = Knex(knexConfig)

export class UserService {
    constructor(private client: Client) { }



    async getUserByEmail(email: string): Promise<User>{
        const result = await this.client.query(/*sql*/ 
            `Select * from users where email = $1;`, [email]
            );

        // console.log(result.rows[0])
        return result.rows[0];



        // const result = await knex.select('*')
        //                     .from("users")
        //                     .where("email", email)

        // return result[0];
    }
    async addUserData(username: string, password: string, email: string, fileName: string) {
        let originalPassword: string = password;
        let hashedPassword = await hashPassword(originalPassword);




        if (fileName) {
            await this.client.query(
                `INSERT INTO users (username,password,image, email,created_at,updated_at) values ($1,$2,$3,$4, NOW(), NOW())`,
                [username, hashedPassword, fileName, email]
            );
        } else {
            await this.client.query(
                `INSERT INTO users (username,password, email,created_at,updated_at) values ($1,$2,$3, NOW(), NOW())`,
                [username, hashedPassword, email]
            );
        }
    }
}
