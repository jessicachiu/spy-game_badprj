
import { Request, Response } from 'express';
import { ai_players_list, game_room_list, getAiPlayerByPlayerID, getGameRoomByRoomID, getPlayerByPlayerID, players_list } from '../helpers/game-Util';
import { createResultJSON } from '../helpers/response-Util';

import { AiPlayer } from '../models/AiPlayer';
import { AiPlayers } from '../models/AiPlayers';
import { Game } from '../models/Game';
import { GameRoom } from '../models/GameRoom';
import { io } from '../server';
import { GameService } from '../services/game-services';


let room_order = 1;

export class GameController {
    constructor(private gameService: GameService) {
        
    }

    getRequestPlayer = (req: Request, res: Response) => {
        // console.log(req.session['cur_player'])

        if (req.session && req.session['cur_player']) {
            res.json(createResultJSON({ player: req.session["cur_player"] }));
            return;
        } else {
            res.status(400).json(createResultJSON(null, { message: "You are not online yet" }));
            return;
        }
    }

    updateRequestPlayer = (req: Request, res: Response) =>{
        let player_id = req.session['cur_player'].id;
        if(!player_id){
            res.json(createResultJSON(null, new Error ('Invalid player id')))
        }
        let updated_player = this.gameService.getCurrentplayer(player_id)
    
        if(updated_player){
            req.session['cur_player'] = updated_player?.get_player();
            res.json(createResultJSON({message:"player_updated"}))
        }else{
            res.status(404).json(createResultJSON(null, {message: "No player on req session"}));
        }
    
    }

    createNewGameRoom= (req: Request, res: Response) =>{
        let  {n_player_number, spy_number, ai_number, round_count} = req.body;
        let room_id= "";
        
        do{
            // room_id = room_id + "+1"
            // room_id = rand_string(5)
            room_id=`aaaaa${room_order}`
            room_order+=1;
        }while( getGameRoomByRoomID(room_id) != null)
        
        let n_player_number_tobe = parseInt(n_player_number, 10)
        let spy_number_tobe = parseInt(spy_number, 10);
        let round_count_tobe = parseInt(round_count,10);
        let ai_number_tobe = parseInt(ai_number,10);
        
        // console.log({round_count_tobe})
        // console.log({ai_number_tobe})
    
        let new_game_room_obj: GameRoom = {
            id: room_id,
            roundCount: round_count_tobe,
            nPlayerNum: n_player_number_tobe,
            spyNum: spy_number_tobe,
            normalAns: "spiderman",
            spyAns: "batman",
            player_id_list:[],
            ready_player_number:0,
    
            normal_player_id_list:[],
            spyPlayer_id_list:[],
    
            game_phase: "unready",
            vote_count: 0,
            cur_game_round: 1,
            cur_answered_player_count:0,
            dead_spy_count:0,
            dead_normal_player_count:0,
    
            ai_number: ai_number_tobe,
            ai_id_list:[],
            
        }
    
        let new_game_room: Game = new Game(new_game_room_obj)
        game_room_list.push(new_game_room)
    
        // console.log(game_room_list)
        let new_game = new_game_room.get_game_room();
    
        // let ai_number = new_game_room.get_ai_number();
    
        console.log({ai_number_tobe})
        for(let j = 1; j <= ai_number_tobe; j++){
            let ai_id = `room_${new_game_room.get_game_room_id()}_ai_${j}`
    
            let ai_player: AiPlayer = {
                id: ai_id,
                name: ai_id,
                room_id: `${room_id}`,
                icon_name: `elephant_icon.jpg`,
                player_vote_count: 0,
                player_answer_list : [],
                player_role_answer : "normal",
                vote_record_id_array: [],
                is_dead: false,
                // is_ai: true,
                isReady: true,
                ai_describe_list:[],
                ai_answered_list:[],
            }
    
            new_game_room.add_ai_id_list(ai_id);
            new_game_room.add_player_id_list(ai_id);
    
            let new_ai_player: AiPlayers = new AiPlayers(ai_player)
            ai_players_list.push(new_ai_player)
            
            
            // io.to(new_game_room.get_game_room_id()).emit("new_player_online",(ai_player))
            // io.to(new_game_room.get_game_room_id()).emit("player_ready")
            // new_player_join_lobby
    
            io.to(new_game_room.get_game_room_id()).emit("new_player_join_lobby", ai_player)
            io.to(new_game_room.get_game_room_id()).emit("player_ready", ai_id)
    
        }
    
        res.status(200).json(createResultJSON({new_game}));

    }

    getCurrentGameRoom = (req:Request, res:Response)=>{
        let room_id = req.session["cur_player"].room_id;
    let game_obj = getGameRoomByRoomID(room_id)


    if(game_obj){
        let game = game_obj.get_game_room();
        res.json(createResultJSON({game}))
    }else{
        res.json(createResultJSON( null,{message: "No Game found"}))
    }

    }


    initGameRoom = (req:Request, res:Response)=>{

        const {room_id} = req.body
        console.log(room_id)
    
        let return_list = []
    
        for(let ai_player of ai_players_list){
            let ai_room_id = ai_player.get_player_room_id()
            if(ai_room_id == room_id){
                return_list.push(ai_player.get_player())
            }
        }
    
        for(let player of players_list){
            let player_room_id = player.get_player_room_id()
            console.log(player_room_id)
            if(player_room_id == room_id){
                return_list.push(player.get_player())
            }
        }
    
        res.json(createResultJSON({return_list}))

    }

    getReadyPlayer = (req : Request, res: Response)=>{
        let player_id_list:string[] = [];
    let room_id = req.session["cur_player"].room_id;
    
    for(let player of ai_players_list){
        let player_room_id = player.get_player_room_id();
        if(player_room_id == room_id && player.get_is_ready()){
            player_id_list.push(player.get_player_id())
        } 
    }

    for(let player of players_list){
        let player_room_id = player.get_player_room_id();
        if(player_room_id == room_id && player.get_is_ready()){
            player_id_list.push(player.get_player_id())
        }
    }

    res.json(createResultJSON({player_id_list}))
    }


    getRecapAnswer = (req : Request, res: Response)=>{
        let recap_data_list = [];

        let player_obj_id = req.session["cur_player"].id;
        let player_obj = getPlayerByPlayerID(player_obj_id) || getAiPlayerByPlayerID(player_obj_id);
    
        if(player_obj){
            let room = getGameRoomByRoomID(player_obj.get_player_room_id());
            if(room){
                let player_list = room.get_player_id_list();
                console.log(player_list)
    
                for(let player_id of player_list){
                    let player = getPlayerByPlayerID(player_id) || getAiPlayerByPlayerID(player_id);
    
                    // console.log(player_id)
                    if(player?.get_is_dead() !=true){
                        let game_round = (room.get_cur_game_round()) -1;
                        let player_answer = player?.get_player_round_answer(game_round);
                        // console.log(player_answer)
        
                        let recap_data = {player_id: player_id, player_answer: player_answer}
                        recap_data_list.push(recap_data)
                    }
                }
                res.json(createResultJSON({data: recap_data_list}))
            }
        }
    }


   getPlayerById = (req:Request, res:Response)=>{
        const{player_id} = req.body;

    let player_obj = getPlayerByPlayerID(player_id) || getAiPlayerByPlayerID(player_id);
    if(player_obj){
        let player_return = player_obj.get_player();
        res.json(createResultJSON({player_return}))
    }else{
        res.status(400).json(createResultJSON(null,{msg: "No player found!!"}))
    }
    }


}
