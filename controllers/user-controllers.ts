import { Request, Response } from 'express';
import { checkPassword } from '../helpers/hash';
import { createResultJSON } from '../helpers/response-Util';

import { UserService } from '../services/user-services';




export class UserController {
    constructor(private userService: UserService) { }


    logout = async (req: Request, res: Response) => {
        delete req.session["user"];
        res.json(createResultJSON("logout success"))
    }

    getUser = async (req: Request, res: Response) => {
        if (req.session && req.session["user"]) {
            res.json(createResultJSON({ user: req.session["user"] }));
            return;
        }
        res.status(400).json(createResultJSON(null, { message: "No login user" }));
    };

    login = async (req: Request, res: Response) => {
        const { email, password } = req.body;

        let userResult = await this.userService.getUserByEmail(email)
        const foundUser = userResult;

        //Username checking--------------------
        if (!foundUser) { res.status(400).json(createResultJSON(null, { message: "email not exist", })); }


        //Password checking--------------------
        // function checkPassword(a: string, b: string) {
        //     if (a == b) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // };
        let isPasswordValid = checkPassword(password, foundUser.password!);


        if (!isPasswordValid) {
            delete req.session["user"];
            res.status(400).json(createResultJSON(null, { message: "login fail" }));
            return;
        }

        if (foundUser) {
            let user = foundUser;
            delete user.password;
            req.session["user"] = user;
            res.json(createResultJSON({ message: "login success", foundUser }));
        }

    };

    register = (req: Request, res: Response) => {
        // async (req: Request, res: Response) => {
            const { username, password, email } = req.body;
            const image = req.file;
            if ( !password || !email) {
                res.status(400).json(
                    createResultJSON({
                        message: "invalid input",
                    })
                );
                return;
            }
            let fileName = image ? image.filename : "";
            this.userService.addUserData(username, password, email, fileName);

            res.json(
                createResultJSON({
                    message: "register success",
                })
            );
        }
    }
