import { Client } from "pg";
import { GameController } from "../controllers/game-controllers";
import { UserController } from "../controllers/user-controllers";
import { GameService } from "../services/game-services";
import { UserService } from "../services/user-services";
import { createUserRoutes } from "./users-routes";
import type {Express} from 'express'
import { createGameRoutes } from "./game-routes";
import { createConfigRoutes } from "./config-routes";

export interface RoutesOption {
    app: Express,
    client : Client

}

export function createRoutes(routesOption : RoutesOption){
    let {
        app,
        client
    
    } = routesOption

    let userService = new UserService(client);
    let userController = new UserController(userService);
    let gameService = new GameService(client);
    let gameController = new GameController(gameService);

    let userRoutes = createUserRoutes(userController)
    let gameRoutes = createGameRoutes(gameController)
    let configRoutes = createConfigRoutes()


    app.use(userRoutes)
    app.use(gameRoutes)
    app.use(configRoutes)
    
}