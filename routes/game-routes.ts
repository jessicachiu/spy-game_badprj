

import express from "express";
import { GameController } from "../controllers/game-controllers";

export function createGameRoutes(gameController : GameController){

    const gameRoutes = express.Router();

    //-----new routes with MVP-----------------------------------------
    gameRoutes.get("/request_player",gameController.getRequestPlayer)
    gameRoutes.put("/request_player", gameController.updateRequestPlayer)
    gameRoutes.post('/new_game_room' , gameController.createNewGameRoom)
    gameRoutes.get('/current_game_room', gameController.getCurrentGameRoom)
    gameRoutes.post('/player_list_with_room_id', gameController.initGameRoom)
    gameRoutes.get('/ready_player', gameController.getReadyPlayer)
    gameRoutes.get('/recap_answer',gameController.getRecapAnswer)
    gameRoutes.post('/player_by_id', gameController.getPlayerById)

    return gameRoutes

}




