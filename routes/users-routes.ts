

import express from "express";


import { multerSingleImage } from "../helpers/multer";
import type { UserController } from "../controllers/user-controllers";


export function createUserRoutes( userController : UserController){
    const userRoutes = express.Router();

    //-----new routes with MVP-----------------------------------------
    userRoutes.post("/logout",userController.logout)
    userRoutes.get("/user", userController.getUser);
    userRoutes.post("/login", userController.login);
    userRoutes.post("/register", multerSingleImage, userController.register);
    return userRoutes
}



