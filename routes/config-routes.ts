
import express from 'express'
import env from '../env'
import { createResultJSON } from '../helpers/response-Util'

export function createConfigRoutes() {

    let configRouter = express.Router()

    configRouter.get("/ai_server", (req, res) => {
        res.json(createResultJSON(env.ai_host))
    })

    return configRouter

}