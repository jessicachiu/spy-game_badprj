import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    
    const hasTable = await knex.schema.hasTable("game_topic");
    if(!hasTable){

        await knex.schema.createTable("game_topic", (table)=>{
            /*
                CREATE TABLE game_topic (
                    id SERIAL PRIMARY KEY,
                    spy_ans string NOT NULL,
                    normal_ans string NOT NULL
                );
            */

            table.increments(); // id SERIAL PRIMARY KEY
            table.string("spy_ans").notNullable; //spy_ans string NOT NULL,
            table.string("normal_ans").notNullable; //normal_ans string NOT NULL
        
        });

    }


}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("game_topic")
}

