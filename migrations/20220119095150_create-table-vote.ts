import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("vote");
    if(!hasTable){
        await knex.schema.createTable("vote", (table)=>{
            /*
                CREATE TABLE vote(
                    id SERIAL primary key,
                    game_room_id integer NOT NULL,
                    round_num integer NOT NULL,
                    voter_player_id integer NOT NULL,
                    votee_player_id integer NOT NULL,
                    created_at timestamp  with time zone NOT NULL
                );
            */ 

                    
                    table.increments(); // id SERIAL primary key,
                    table.integer("game_room_id").notNullable;  //game_room_id integer NOT NULL
                    table.integer("round_num").notNullable;
                    table.integer("voter_player_id").notNullable;
                    table.integer("votee_player_id").notNullable;
                    table.timestamp('created_at').defaultTo(knex.fn.now());
                    
        });

    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("vote")
}

