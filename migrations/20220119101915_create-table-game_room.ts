import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    const hasTable = await knex.schema.hasTable("game_room");
    if(!hasTable){
        await knex.schema.createTable("game_room", (table) => {
            /*
                CREATE TABLE game_room(
                    id SERIAL PRIMARY key,
                    round_count integer NOT NULL,
                    n_player_num integer NOT NULL,
                    spy_num integer NOT NULL,

                    game_topic_id integer NOT NULL,
                    FOREIGN KEY (game_topic_id) REFERENCES game_topic(id),
                    
                    voteing_time integer NOT NULL,
                    gaming_time integer NOT NULL,
                    winning_role VARCHAR(255) not NULL
                );
            
            */

                table.increments();
                table.integer("round_count").notNullable;
                table.integer("n_player_num").notNullable;
                table.integer("spy_num").notNullable;

                table.integer("game_topic_id").notNullable;
                table.foreign("game_topic_id").references("game_topic.id");

                table.integer("voting_time");
                table.integer("gaming_time");
                table.string("winning_role");


        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("game_room");
}

