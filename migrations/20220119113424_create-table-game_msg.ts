import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    
    const hasTable = await knex.schema.hasTable("game_msg");
    if(!hasTable){
        await knex.schema.createTable("game_msg", (table) => {
            /*
                Create TABLE game_msg(
                    id SERIAL PRIMARY KEY,

                    game_room_id integer NOT NULL,
                    FOREIGN KEY (game_room_id) REFERENCES game_room(id),

                    round_num integer NOT NULL,
                    game_phase VARCHAR(255) NOT NULL,
                    roles VARCHAR(255) NOT NULL,
                    player_id integer NOT NULL,
                    user_id integer,
                    content VARCHAR(255) NOT NULL,
                    created_at timestamp  with time zone NOT NULL
                );
            
            */

                table.increments();

                table.integer("game_room_id").notNullable;
                table.foreign("game_room_id").references("game_room.id");

                table.integer("round_num").notNullable;
                table.string("game_phase").notNullable;
                table.string("roles").notNullable;
                table.integer("player_id").notNullable;
                table.integer("user_id");
                table.string("content").notNullable;
                table.timestamp('created_at').defaultTo(knex.fn.now());

        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("game_msg")
}



