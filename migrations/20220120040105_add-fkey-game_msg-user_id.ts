import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('game_msg')){
        await knex.schema.alterTable('game_msg',(table)=>{
            /*
                ALTER TABLE game_msg ADD FOREIGN KEY (user_id) REFERENCES users(id);
            */
            table.foreign("user_id").references("users.id");
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("game_msg")){
        await knex.schema.alterTable('game_msg',(table)=>{
            /*
                ALTER TABLE game_msg DROP CONSTRAINT game_msg_user_id_foreign;
            */
            table.dropForeign("user_id")
        });
    }
}

