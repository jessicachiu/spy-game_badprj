import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('vote')){
        await knex.schema.alterTable('vote',(table)=>{
            /*
                ALTER TABLE vote ADD FOREIGN KEY (game_room_id) REFERENCES game_room(id);
            */
            table.foreign("game_room_id").references("game_room.id");
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("vote")){
        await knex.schema.alterTable('vote',(table)=>{
            /*
                ALTER TABLE vote DROP CONSTRAINT vote_game_room_id_foreign;
            */
            table.dropForeign("game_room_id")
        });
    }
}

