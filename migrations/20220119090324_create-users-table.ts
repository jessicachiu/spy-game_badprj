import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    
    const hasTable = await knex.schema.hasTable("users");
    if(!hasTable){
        await knex.schema.createTable("users", (table) => {
            /*
                CREATE TABLE users(
                    id SERIAL primary key,
                    email VARCHAR(255) NOT NULL,
                    username VARCHAR(255),
                    password VARCHAR(255) NOT NULL,
                    created_at timestamp with time zone NOT NULL,
                    updated_at timestamp with time zone NOT NULL
                );
            */ 

            table.increments(); // id SERIAL primary key,
            table.string("username"); // username VARCHAR(255)
            table.string("email").notNullable; //email VARCHAR(255) NOT NULL,
            table.string("password").notNullable; // password VARCHAR(255) NOT NULL,
            table.timestamps(false, true); // created_at  , updated_at
        });
    }
    
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("users");
}