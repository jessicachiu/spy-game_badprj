

CREATE TABLE users(
    id SERIAL primary key,
    email VARCHAR(255) NOT NULL,
    username VARCHAR(255),
    password VARCHAR(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);

CREATE TABLE game_room(
    id SERIAL primary key,
    round_count integer NOT NULL,
    n_player_num integer NOT NULL,
    spy_num integer NOT NULL,
    normal_ans string NOT NULL,
    spy_ans VARCHAR(255) NOT NULL,
    game_topic_id integer NOT NULL,
    voting_time integer NOT NULL,
    gaming_time integer NOT NULL,
    winning_role VARCHAR(255) NOT NULL
);

--DROP TABLE users;

Select * from users;



INSERT INTO users 
    (username, PASSWORD, email, created_at ,updated_at) 
    VALUES
    ('John_2', '22222', '222@222.com', NOW(), NOW());


select * from users where email = '111@111.com';


CREATE TABLE vote(
    id SERIAL primary key,
    game_room_id integer NOT NULL,
    round_num integer NOT NULL,
    voter_player_id integer NOT NULL,
    votee_player_id integer NOT NULL,
    created_at timestamp  with time zone NOT NULL
);


CREATE TABLE game_room(
    id SERIAL PRIMARY key,
    round_count integer NOT NULL,
    n_player_num integer NOT NULL,
    spy_num integer NOT NULL,
    game_topic_id integer NOT NULL,
    FOREIGN KEY (game_topic_id) REFERENCES game_topic(id),
    voteing_time integer NOT NULL,
    gaming_time integer NOT NULL,
    winning_role VARCHAR(255) not NULL
);

--drop table gmae_room;


Create TABLE game_msg(
    id SERIAL PRIMARY KEY,

    game_room_id integer NOT NULL,
    FOREIGN KEY (game_room_id) REFERENCES game_room(id),

    round_num integer NOT NULL,
    game_phase VARCHAR(255) NOT NULL,
    roles VARCHAR(255) NOT NULL,
    player_id integer NOT NULL,
    user_id integer,
    content VARCHAR(255) NOT NULL,
    created_at timestamp  with time zone NOT NULL


);



ALTER TABLE vote ADD CONSTRAINT vote_game_room_id_foreign FOREIGN KEY (game_room_id) REFERENCES game_room(id);

ALTER TABLE vote DROP CONSTRAINT vote_game_room_id_foreign;


ALTER TABLE game_msg ADD FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE game_msg DROP CONSTRAINT game_msg_user_id_foreign;







