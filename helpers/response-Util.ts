export function createResultJSON(data: any | undefined = {}, err: any | undefined = {}) {
    return {
        data,
        err,
    };
}
