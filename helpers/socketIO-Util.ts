import socketIO from "socket.io";
import { Request } from 'express'
import * as gemeUtil from './game-Util'
import { get_random_int, rand_string } from "./gen_random";
import env from "../env";
import { players_list } from "./game-Util";
import type { Player } from "../models/Player";
import { Players } from "../models/Players";
import { AiPlayers } from "../models/AiPlayers";
import fetch from 'node-fetch';


export let io: socketIO.Server;
export function setSocketIO(value: socketIO.Server) {
    let io = value;
    io.on('connection', function (socket) {
        const req = socket.request as Request;
        req.session['socket_id'] = socket.id;

        console.log("req.session['socket_id']: ", req.session['socket_id'])


        if (req.session && req.session["cur_player"]) {
            let player = req.session["cur_player"]
            socket.join(`${player.room_id}`)
        }


        socket.on("new_player_online", async (player: Player) => {
            // *******
            rand_string(10);
            // player.id = rand_string(10);
            player.id = player.name;
            // *******

            req.session["cur_player"] = player;
            console.log(req.session)

            let new_player: Players = new Players(player)

            // players_list.push(new_player);
            // console.log({players_list})


            let room = gemeUtil.getGameRoomByRoomID(player.room_id);

            if (room == undefined) {
                socket.emit("no_game_room")
            } else if (room) {
                room.add_player_id_list(player.id)
                players_list.push(new_player);
                console.log({ players_list })


                socket.join(`${player.room_id}`) //saving the roomid in socket["room"]
                // console.log({game_room_list})
                socket.request["session"].save();

                io.to(`${player.room_id}`).emit("new_player_join_lobby", {
                    player
                })
            }
        })

        socket.request["session"].save();


        io.emit('connected-socket-id', socket.id)




        socket.on("game_chat_msg", (msg) => {
            console.log("text submitted to server through socket")

            console.log(req.session["cur_player"])

            io.to(req.session["cur_player"].room_id).emit("game_chat_msg", {
                msg: msg,
                from: req.session["cur_player"]
            })
        })


        socket.on("player_ready", (player_id) => {
            let player = gemeUtil.getPlayerByPlayerID(player_id) || gemeUtil.getAiPlayerByPlayerID(player_id);

            if (player) {
                player.set_player_ready();

                let game = gemeUtil.getGameRoomByRoomID(player?.get_player_room_id())
                if (game) {
                    game.player_ready();
                    if (game.get_total_player_number() <= game.get_ready_player_number()) {
                        io.to(req.session["cur_player"].room_id).emit("all_player_ready");
                    }

                    // if(0 < game.get_ready_player_number()){
                    //     io.to(req.session["cur_player"].room_id).emit("all_player_ready");
                    // }
                }

                console.log(player)
                io.to(req.session["cur_player"].room_id).emit("player_clicked_ready", {
                    player_id: player_id
                })
            }
        })


        //next round action for ai





        // Entering game room------------------------
        socket.on("to_game_room", async () => {
            let room = gemeUtil.getGameRoomByRoomID(req.session["cur_player"].room_id)
            // new_game_room.add_normal_player_id_list(ai_id); // Preset all ai_player to be normal player

            if (room) {

                //----- Set all ai as normal player
                let ai_player_id_list = room.get_ai_id_list();

                console.log({ ai_player_id_list })

                for (let ai_player_id of ai_player_id_list) {
                    let ai_player = gemeUtil.getAiPlayerByPlayerID(ai_player_id)
                    ai_player?.set_player_role_answer(room.get_normal_answer())
                    ai_player?.set_player_role("normal")
                    room.add_normal_player_id_list(ai_player_id)

                    //ai_ask sanic server for ai answer list

                    let res = await fetch(`${env.ai_host}/noun`, {
                        // let res = await fetch(`http://192.168.59.64:8108/noun`,{
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json;",
                        },
                        body: JSON.stringify({ "noun": room.get_normal_answer() })
                    })

                    let adj_str_raw = await res.json();
                    let myRe = /\w+/g;
                    let adj_array = adj_str_raw.match(myRe);

                    ai_player?.set_ai_describe_list(adj_array) //string[]

                    console.log(ai_player)
                }





                // let normal_player_num = room.get_normal_player_num();
                let spy_num = room.get_spy_player_num();

                let room_player_num = room.get_total_player_number_without_ai();
                let room_player_id_list = room.get_player_id_list_without_ai();

                for (let i = 0; i < spy_num; i++) {
                    let chose_spy_index = (get_random_int(1, room_player_num) - 1)
                    // console.log({chose_spy_index})

                    let new_spy_id = room_player_id_list[chose_spy_index];
                    room.add_spy_player_id_list(new_spy_id);

                    let player = gemeUtil.getPlayerByPlayerID(new_spy_id);
                    player?.set_player_role("spy")
                    player?.set_player_role_answer(room.get_spy_answer());
                    // console.log("spy player: ",player)

                }


                let spy_player_id_list = room.get_spy_player_list();


                let temp_list = room_player_id_list.filter(item => !spy_player_id_list.includes(item))

                console.log({ temp_list })

                for (let player_id of temp_list) {
                    room.add_normal_player_id_list(player_id)

                    let player = gemeUtil.getPlayerByPlayerID(player_id);
                    player?.set_player_role("normal")
                    player?.set_player_role_answer(room.get_normal_answer());
                    // console.log("normal player= ",player)
                }






                console.log("normal player id list: ", room.get_normal_player_list())
                console.log("spy player id: ", room.get_spy_player_list())

                room.set_game_phase_gaming();
                setTimeout(() => {
                    if (room) {
                        //$$$game play begin start with gaming
                        io.to(room.get_game_room_id()).emit("next_round", {
                            next_round: room.get_cur_game_round(),
                            game_phase: room.get_game_phase(),
                        })


                    }
                }, 500)

                //ai_speaks describe here for the first time
                let room_ai_number = room.get_ai_number()

                for (let j = 1; j <= room_ai_number; j++) {
                    setTimeout(() => {

                        if(room){
                            let ai_player = gemeUtil.getAiPlayerByPlayerID(`room_${room?.get_game_room_id()}_ai_${j}`)
                            let answer = ai_player?.get_set_ai_new_answer();

                            room.add_cur_answered_player_count();

                            io.to(req.session["cur_player"].room_id).emit("game_chat_msg", {
                                msg: answer,
                                from: ai_player?.get_player(),
                            })
                        }

                    }, j * 1000)
                }

            }

            io.to(req.session["cur_player"].room_id).emit("to_game_room")

        })

        socket.on("player_describe", (description) => {
            let player_id = req.session["cur_player"].id;
            let player = gemeUtil.getPlayerByPlayerID(player_id) || gemeUtil.getAiPlayerByPlayerID(player_id)

            if (!player) {
                socket.emit("Error: no player!!")
                return
            }


            let room = gemeUtil.getGameRoomByRoomID(player.get_player_room_id());
            if (!room) {
                return
            }

            room.add_cur_answered_player_count();
            player.add_player_answer_list(description);

            io.to(req.session["cur_player"].room_id).emit("game_chat_msg", {
                msg: description,
                from: req.session["cur_player"]
            })

            let cur_answered_player = room.get_cur_answered_player_count();

            if (cur_answered_player >= room.get_survive_player_number()) {
                // all player described
                room.set_game_phase_voting();

                //$$$AI-phase change to voting here
                let room_ai_number = room.get_ai_number()

                io.to(room.get_game_room_id()).emit("phase_change", {
                    phase: room.get_game_phase(),
                    round: room.get_cur_game_round(),
                    room_id: room.get_game_room_id(),
                })

                for (let j = 1; j <= room_ai_number; j++) {
                    setTimeout(() => {
                        if(room){
                            let ai_player = gemeUtil.getAiPlayerByPlayerID(`room_${room?.get_game_room_id()}_ai_${j}`)
                            let is_ai_dead = ai_player?.get_is_dead();

                            if (is_ai_dead != true) {
                                ai_player?.add_vote_record_id_array("skipVote_id");

                                room.add_vote_count();

                                io.to(room.get_game_room_id()).emit("game_chat_msg", {
                                    msg: "(Game Message: Voted)",
                                    from: ai_player?.get_player(),
                                })

                            }
                        }
                        
                    }, j * 1000)
                }
            }
        })

        socket.on("player_vote", async (voted_to_id) => {
            let player_id = req.session["cur_player"].id;
            let player = gemeUtil.getPlayerByPlayerID(player_id) || gemeUtil.getAiPlayerByPlayerID(player_id);
            let game_end_by_vote: boolean = false;
            let winning_role: string | null = null;


            if (player) {
                let room = gemeUtil.getGameRoomByRoomID(player.get_player_room_id());
                if (room) {

                    room.add_vote_count();
                    player.add_vote_record_id_array(voted_to_id);

                    console.log("voted_to: ", player.get_vote_record_by_round(0))

                    io.to(player.get_player_room_id()).emit("game_chat_msg", {
                        msg: "(Game Message: Voted)",
                        from: req.session["cur_player"]
                    })


                    if (room.get_vote_count() >= room.get_survive_player_number()) {
                        console.log("** All player shoule have been voted")
                        //From here all player voted!!
                        //Count vote here instead of game_room.js

                        //Get voting data[{voter: player_id, votee: player_id}]
                        //--socket send recap answer here!!
                        let vote_record = gemeUtil.getRecapVoteListByRoomId(room.get_game_room_id());

                        // console.log("vote_record at player_vote: ", vote_record); 

                        console.log("vote_record--(before): ", vote_record)



                        //--Calculate vote result
                        let dead_player: Players | AiPlayers | null | undefined;

                        let votee_array: string[] = [];

                        //--**Splice and slice all skipVote to another array;
                        let skipped_vote_record = vote_record.filter(record => record.vote_to == 'skipVote_id')
                        skipped_vote_record.forEach((skip_record) => {
                            vote_record.splice(
                                vote_record.findIndex(
                                    vote_rec =>
                                        vote_rec == skip_record

                                ), 1)
                        })

                        console.log("Skipped vote record: ", skipped_vote_record)
                        console.log("vote_record--(after): ", vote_record)



                        vote_record.forEach((record) => { record.vote_to ? votee_array.push(`${record.vote_to}`) : null })
                        let highest_vote_array = gemeUtil.mode(votee_array); //--An "ID-LIST" with the same highest vote






                        if (highest_vote_array.length > 1) {
                            //---Nobody dies, equal amount of vote among different ppl
                            //--Game continues
                            console.log("Nobody dies this round, same vote count")
                            dead_player = undefined;

                        } else if (highest_vote_array.length == 1) {
                            //---One people die this round

                            dead_player = gemeUtil.getPlayerByPlayerID(highest_vote_array[0]) || gemeUtil.getAiPlayerByPlayerID(highest_vote_array[0]); // set deadplayer from list result
                            if (dead_player) {
                                dead_player?.set_is_dead(true);
                                //emit to the specific player that that player is dead

                                if (dead_player.get_is_dead() == true) {
                                    if (dead_player.get_player_role() == 'spy') {
                                        room.add_dead_spy_count();
                                    } else if (dead_player.get_player_role() == 'normal') {
                                        room.add_dead_normal_player_count();
                                    }
                                }

                                if (room.is_normal_player_win()) {
                                    // io.to(dead_player.get_player_room_id()).emit("normal_player_win")
                                    winning_role = "normal"
                                    game_end_by_vote = true;

                                } else if (room.is_spy_win()) {
                                    // io.to(dead_player.get_player_room_id()).emit("spy_win")
                                    winning_role = "spy"
                                    game_end_by_vote = true;

                                } else {
                                    io.to(dead_player.get_player_room_id()).emit("player_is_dead", (dead_player.get_player_id()))
                                }

                            }


                        } else if (skipped_vote_record.length > 1) {
                            dead_player = undefined;
                            console.log("Nobody dies this round, All skipped vote")
                        } else {
                            //error on counting mode
                            console.log("Error: vote counting mode error")
                        }

                        //--Phase Change if game not ended
                        room.round_end();
                        room.set_game_phase_gaming();
                        room.reset_vote_count();
                        room.reset_cur_answered_player_count();

                        if (room.get_game_phase() == "Gaming") {
                            console.log("phase change to gaming")
                            setTimeout(() => {
                                if (room) {
                                    io.to(room.get_game_room_id()).emit("phase_change", {
                                        phase: room.get_game_phase(),
                                        round: room.get_cur_game_round(),
                                        vote_record: vote_record,
                                        dead_player: dead_player,
                                        skipped_vote_record: skipped_vote_record,
                                    })


                                    //emit winning message after calculation
                                    if (winning_role == 'normal' && dead_player) {
                                        io.to(dead_player.get_player_room_id()).emit("normal_player_win")
                                    } else if (winning_role == 'spy' && dead_player) {
                                        io.to(dead_player.get_player_room_id()).emit("spy_win")
                                    }
                                }
                            }, 200)
                        } else {
                            io.to(room.get_game_room_id()).emit("phase_change", {
                                phase: room.get_game_phase(),
                                round: room.get_cur_game_round(),
                            })
                        }

                        //--NextRound for game that continues
                        if (room.is_game_ended() == false) {
                            setTimeout(() => {
                                if (room) {
                                    io.to(room.get_game_room_id()).emit("next_round", {
                                        next_round: room.get_cur_game_round(),
                                        game_phase: room.get_game_phase(),
                                        room_id: room.get_game_room_id(),
                                    })
                                }
                            }, 600)

                            //ai_speaks describe here if game citinues
                            let room_ai_number = room.get_ai_number()

                            for (let j = 1; j <= room_ai_number; j++) {
                                setTimeout(() => {
                                    if (room) {
                                        let ai_player = gemeUtil.getAiPlayerByPlayerID(`room_${room?.get_game_room_id()}_ai_${j}`)
                                        let is_ai_dead = ai_player?.get_is_dead();

                                        if (is_ai_dead != true) {
                                            let answer = ai_player?.get_set_ai_new_answer();

                                            room.add_cur_answered_player_count();

                                            io.to(req.session["cur_player"].room_id).emit("game_chat_msg", {
                                                msg: answer,
                                                from: ai_player?.get_player(),
                                            })
                                        }

                                    }
                                }, j * 1000)
                            }
                            //ai_speaks describe here if game continues

                        } else {
                            room.set_game_phase_end();
                            setTimeout(() => {
                                if (room) {
                                    if (game_end_by_vote != true) {
                                        io.to(room.get_game_room_id()).emit("spy_win") // spy wins as spy survived!
                                    }
                                    io.to(room.get_game_room_id()).emit("game_ended")
                                }
                            }, 200)
                        }


                    }

                }
            }

        })


        socket.on("disconnect", () => {
            console.log(`Socket disconnected: ${req.session['socket_id']}`)
        })

    });

}