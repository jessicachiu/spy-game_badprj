import { AiPlayers } from "../models/AiPlayers";
import { Game } from "../models/Game";
import { Players } from "../models/Players";

export let players_list:Players[] = [];
export let game_room_list:Game[] = [];
export let ai_players_list:AiPlayers[] = [];


export function getPlayerByPlayerID(player_id:string){
    let player_obj;
    for(let player of players_list){
        let player_obj_id = player.get_player_id();
        if(player_obj_id == player_id){
            player_obj = player;
        }
    }
    return player_obj;
}

export function getAiPlayerByPlayerID(ai_player_id:string){
    let ai_player_obj;
    for(let ai_player of ai_players_list){
        let ai_player_obj_id = ai_player.get_player_id();
        if(ai_player_obj_id == ai_player_id){
            ai_player_obj = ai_player;
        }
    }
    return ai_player_obj;
}

export function getGameRoomByRoomID(room_id: string) {
    let game_obj;
    for(let game of game_room_list){
        let game_room_id = game.get_game_room_id();
        if(game_room_id == room_id){
            game_obj = game;
        }
    }
    console.log(game_obj)
    return game_obj;

}

export function getRecapVoteListByRoomId(room_id:string){
    let recap_vote_list = [];
    let room = getGameRoomByRoomID(room_id);
    if(room){
        let player_list = room.get_player_id_list();
        for(let player_id of player_list){
            let player = getPlayerByPlayerID(player_id) || getAiPlayerByPlayerID(player_id)
            if(player?.get_is_dead() != true){
                let game_round = (room.get_cur_game_round() - 1);
                // console.log("game round: ",game_round)

                let vote_to_player = player?.get_vote_record_by_round(game_round)
                // let vote_to_player = player?.get_vote_record_by_round(0)

                let recap_vote = {voter: player_id, vote_to: vote_to_player}
                recap_vote_list.push(recap_vote)
            }
        }
    }
    return recap_vote_list;
}

export function mode(array:any[]) {
    let modes:any[] = []
    let count:any[] = [], maxIndex = 0;

    for (let i = 0; i < array.length; i++) {
        let mode_item = array[i];
        count[mode_item] = (count[mode_item] || 0) + 1;
        if (count[mode_item] > maxIndex) {
            maxIndex = count[mode_item];
        }
    }
    // console.log("count: ", count)

    for (let i in count)
        if (count.hasOwnProperty(i)) {
            if (count[i] === maxIndex) {
                modes.push(`${i}`);
            }
        }

    return modes;
}