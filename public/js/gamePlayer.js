const socket = io.connect();



let room_id = new URL(window.location.href).searchParams.get(`room_id`);

let room_id_show = document.querySelector(".room_id_show")
room_id_show.innerHTML = room_id

// console.log("gamePlayerRoom")

let icon_select_list = document.querySelector("#icon_select_list")
let upload_icon_option = document.querySelector("#upload_icon_option")
let profile_pic_icon = document.querySelector(".profile_pic_icon")


icon_select_list.addEventListener("change", (event)=>{
    const value = icon_select_list.value
    if(icon_select_list.value == 'upload_file'){
        upload_icon_option.click();
    }else {
        profile_pic_icon.setAttribute("src", `./assets/photos/default_profile_pic/${value}`)
    }
})

let player_online_form = document.querySelector('#player_online_form')
player_online_form.addEventListener('submit', player_online_form_submission)

async function player_online_form_submission(event){
    event.preventDefault();
    let player_name = document.querySelector("#name").value

    let icon_name = icon_select_list.value

    // console.log({player_name})

    if(!player_name){
        // console.log("player name cannot be null")
        let no_name_remainder = document.querySelector(".no_name_remainder")
        no_name_remainder.classList.remove("d-none")
    }else{
        let player = {
            id: ``,
            name: `${player_name}`,
            room_id: `${room_id}`,
            icon_name: `${icon_name}`,
            player_vote_count: 0,
            player_answer_list : [],
            player_role_answer : "",
            vote_record_id_array: [],
            is_dead: false,
            // is_ai: false,
        }
    
        console.log(player)

        socket.emit("new_player_online",player)

        let res = await fetch("/request_player")
        let result = await res.json();

        console.log(result)

        window.location.href= `/waiting_room.html?room_id=${room_id}`
    }


}

// let loginForm = document.querySelector('#login_form').addEventListener('submit', login)
// async function login(event) {
//     event.preventDefault()
//     console.log('sumbit');

//     let playerName = document.querySelector('#player_name').value
//     const res = await fetch(`/login?playerName=${playerName}`, {
//         method: "POST", // Specific your HTTP method
//     })
//     const data = await res.text()
//     document.querySelector('#login-result').innerHTML = data
//     if (res.ok) {
//         window.location.href = `index.html?playerName=${playerName}`
//     }
// }