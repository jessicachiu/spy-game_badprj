document.querySelector("#register-form").addEventListener("submit", async (event) => {
    event.preventDefault();

    let formElem = event.currentTarget;
    
    let formObj = {};
    formObj.username = formElem.username.value;
    formObj.password = formElem.password.value;
    formObj.image = formElem.image.value;
    formObj.email = formElem.email.value;
    let formData = new FormData(formElem);

    let res = await fetch("/register", {
        method: "POST",
        body: formData,
    });

    if (res.ok) {
        alert("Success")
        window.location = "/log_and rig.html";
    }
});