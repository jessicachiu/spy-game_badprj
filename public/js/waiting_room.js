console.log("waiting-room !")

const socket = io.connect();




// let acc_page = new URL(window.location.href).searchParams.get(`acc_page`);


let room_id = new URL(window.location.href).searchParams.get(`room_id`);

console.log(room_id)

let room_id_show = document.querySelector("#room_id_show")



let player_list = document.querySelector(".player_list")
let start_game_btn = document.querySelector("#start_game_btn")



// Add new joiner
socket.on("new_player_join_lobby", async (player)=>{
    // console.log({player})
    let new_player = player.player;
    player_list.insertAdjacentHTML("beforeend", 
    /*html*/`
    <tr id="${new_player.id}">
        <td class="player_icon_col"><img class="player_image" src="./assets/photos/default_profile_pic/${new_player.icon_name}"></td>
        <td class="player_name_col">${new_player.name}</td>
        <td class="status_box"><span class="ready_btn not_ready_btn" id="player_${new_player.id}">Not Ready</span>
    </tr>
    `)
})

//current player clicked ready
let player_ready_btn = document.querySelector("#player_ready_btn")
player_ready_btn.addEventListener("click", async ()=>{
    let res = await fetch("/request_player")
    let result = await res.json();
    let player_id = result.data.player.id

    // console.log(player_id)
    socket.emit("player_ready", (player_id))
    player_ready_btn.classList.add("disabled_btn")
    player_ready_btn.setAttribute("disabled", "")
})


//other player clicked ready
socket.on("player_clicked_ready", (player_id) =>{

    console.log(player_id)
    let status_btn = document.querySelector(`#player_${player_id.player_id}`)
    status_btn.classList.remove("not_ready_btn")
    status_btn.innerHTML=`Ready`


})


// When all player is ready
socket.on("all_player_ready", ()=>{
    start_game_btn.classList.remove("disabled_btn")
    start_game_btn.removeAttribute("disabled")
})

//slick start game btn
start_game_btn.addEventListener("click", ()=>{
    console.log("start_game_btn_clicked")
    socket.emit("to_game_room")
})

socket.on("to_game_room", ()=>{
    window.location= "/game_room.html";
})


async function init(){

    room_id_show.innerHTML = room_id;

    //Load beforehand player
    let res = await fetch("/player_list_with_room_id", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({"room_id":`${room_id}`})
    })

    let result = await res.json();

    let player_list_array = result.data.return_list;
    for(let player of player_list_array){
        player_list.insertAdjacentHTML("beforeend", 
        /*html*/`
        <tr id="${player.id}">
            <td class="player_icon_col"><img class="player_image" src="./assets/photos/default_profile_pic/${player.icon_name}"></td>
            <td class="player_name_col">${player.name}</td>
            <td class="status_box"><span class="ready_btn not_ready_btn" id="player_${player.id}">Not Ready</span>
        </tr>
        `)
    }

    //show ready for readied player
    let res2 = await fetch("/ready_player")
    let result2 = await res2.json();

     let player_id_list = result2.data.player_id_list

    for(let player_id of player_id_list){
        let status_btn = document.querySelector(`#player_${player_id}`)
        status_btn.classList.remove("not_ready_btn")
        status_btn.innerHTML=`Ready`
    
    }
}

init();














