
let loginForm = document.querySelector("#login-form");
document.querySelector("#login-form").addEventListener("submit", async (event) => {
    event.preventDefault();
    let formElem = event.currentTarget;
    let formObj = {};
    formObj.email = formElem.email.value;
    formObj.password = formElem.password.value;

    let res = await fetch("/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObj),
    });
    if (res.ok){
        window.location = "/landing.html";
    }else{
        alert("Wrong input")
    }
})
