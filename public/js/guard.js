import express from "express";

import { client } from "./server";



function createResultJSON(data= {}, err = {}) {
    return {
        data,
        err,
    };
}
export const isLoggedIn = (req, res, next) => {
    if (req.session && req.session["user"]) {
        //called Next here
        next();
    } else {
        // redirect to index page
        console.log("you are not log in , redirect now!!!");
        // res.redirect("/");
        res.status(401).json(createResultJSON(null, { message: "Not Authorized" }));
    }
};