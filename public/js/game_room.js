
const socket = io.connect();

// console.log("game_room.js testing!!")



let game_chat_wrap = document.querySelector(".game_chat_wrap")
let game_recap_main  = document.querySelector(".game_recap_main")


let show_ans_btn = document.querySelector(".show_answer")
let vote_btn = document.querySelector(".vote_btn")

let show_ans_cross_btn = document.querySelector(".show_ans_cross_btn")
let show_ans_ok_btn = document.querySelector(".show_ans_ok_btn")
let voter_cross_btn = document.querySelector(".voter_cross_btn")

let show_ans_answer = document.querySelector(".show_ans_answer")
let show_ans_player_identity = document.querySelector(".show_ans_player_identity")

// let test_noun_form = document.querySelector("#test_noun").addEventListener('submit', server_noun)


let show_answer_wrap_back = document.querySelector(".show_answer_wrap_back")

let voter_wrap_back = document.querySelector(".voter_wrap_back")
let vote_form = document.querySelector("#vote_form")
let voting_main = document.querySelector(".voting_main")

let game_id_value = document.querySelector(".game_id_value")
let game_phase_value = document.querySelector(".game_phase_value")
let game_round_number = document.querySelector(".game_round_number")

let cur_player; //this will be inited by init()
let game_room; //this will be init by init()
let player_list;

let game_phase_main = document.querySelector(".game_phase_main")









// Show answer---------
show_ans_btn.addEventListener("click", () => {
    show_answer_wrap_back.classList.remove("d-none")
})

show_ans_cross_btn.addEventListener("click", () => {
    show_answer_wrap_back.classList.add("d-none")
})

show_ans_ok_btn.addEventListener("click", () => {
    show_answer_wrap_back.classList.add("d-none")
})




// Vote----------------------------------------------------------------------

voter_cross_btn.addEventListener("click", () => {
    voter_wrap_back.classList.add("d-none")
    text_submit_form.reset();
})


vote_btn.addEventListener("click", () => {
    // console.log("vote btn clicked")
    voter_wrap_back.classList.remove("d-none")
})

vote_form.addEventListener("submit", submit_vote)

async function submit_vote(event){
    event.preventDefault();

    let voted_player_value = document.forms['vote_form']["being_voted"].value;
    console.log(voted_player_value)

    if(voted_player_value){
        let being_voted_player_value_checked = document.querySelector('input[name="being_voted"]:checked').value
        let being_voted_player_id = document.querySelector('input[name="being_voted"]:checked').id

        // console.log(`${being_voted_player_value_checked} with id:${being_voted_player_id} has been voted!` )
        socket.emit("player_vote", being_voted_player_id)

        voter_wrap_back.classList.add("d-none")

    }else{
        let vote_reminder = document.querySelector(".vote_reminder")
        vote_reminder.innerHTML = `**Please select player to confirm vote!`

    }
    vote_form.reset();
    disable_vote_btn();
}




// submit text and console.log
let text_submit_btn = document.querySelector(".text_submit_btn")
let text_submit_form = document.querySelector("#text_submit_form")
let submitting_text_input = document.querySelector(".submitting_text");


text_submit_form.addEventListener('submit', submit_text)


// game_logic when submitting text-------
async function submit_text(event){
    event.preventDefault();
    let submitting_text = submitting_text_input.value;
    let game_phase = game_room.game_phase;


    // console.log("game_phase = ",game_room.game_phase)

    if(game_phase == "Gaming"){
        socket.emit("player_describe", (submitting_text))
        disable_input();
    }else{
        emit_msg(submitting_text)
    }


    text_submit_form.reset();
}



async function emit_msg(msg){
    socket.emit("game_chat_msg", msg)
}

function text_direction(sender_id, player_id){
    if(sender_id == player_id){
        return "chat_right"
    }else{
        return "chat_left"
    }
}


// receiving game_msg and description
socket.on("game_chat_msg", async(content)=>{
    console.log(content.msg)
    console.log(content.from)

    let res = await fetch("/request_player")
    let result = await res.json();
    let cur_player = result.data.player;


    let text_dir = text_direction(content.from.id, cur_player.id)
    // console.log(cur_player.icon_name)


    new_text_box(`${content.from.name}`, `${content.msg}`, `${text_dir}`,`${content.from.icon_name}`)
    game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;

})



// receiving game phase change
socket.on("phase_change", async (content)=>{
    // console.log(content.phase)

    setTimeout(phase_change_actions() , 1000)

    async function phase_change_actions() {
        game_room.game_phase = content.phase;
        game_phase_value.innerHTML = game_room.game_phase;
    
        game_round_number.innerHTML = content.round;
    
        if(game_room.game_phase == "Voting"){
            if(cur_player.is_dead == true){
                disable_input();
                disable_vote_btn();
            }else{
                enable_vote_btn();
                enable_input();
            }
            // socket.emit("recap_answer")
            await print_recap_data();
        }else if(game_room.game_phase == "Gaming" || "Ended"){
            await vote_recap_func(content.vote_record , content.dead_player, content.skipped_vote_record);

            if(cur_player.is_dead == true){
                disable_input();
                disable_vote_btn();
            }
        }
    
        await update_data();
    }

})

// recap round message----------------
async function print_recap_data(){
    let res = await fetch("recap_answer")
    let result = await res.json();
    let recap_data_list = result.data.data;

    game_chat_wrap.insertAdjacentHTML("beforeend",
    /*html*/`
        <div class="chat_msg chat_left">
            <div class="chat_avatar">
                <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                    alt="Retail Admin" class="chat_avatar_img">
            </div>

            <div class="chat_main">
                <div class="player_name">Game Master Announcement</div>
                <div class="chat_text announcement">
                    <div class="game_recap_header">Round ${game_room.cur_game_round} recap:</div>
                    <div class="game_recap_main_wrap">
                        <div class="game_recap_main row" id="round_${game_room.cur_game_round}_recap">

                        </div>
                    </div>
                    <div class="dis_time_left">Discussion / Voting Time Left: 30 (s)</div>
                </div>
            </div>
        </div>
    `)

    let recap_div = document.querySelector(`#round_${game_room.cur_game_round}_recap`)


    for(let data of recap_data_list){
        console.log(data)
        // player_answer: "sadsdaCasfxassa"
        // player_id: "Rci0wwHn4V"

        let res2 = await fetch("/player_by_id", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({"player_id": data.player_id})
        })
        let result2 = await res2.json();
        let player = result2.data.player_return;

        console.log(player)

        recap_div.insertAdjacentHTML("beforeend", 
        /*html*/`
            <div class="player_recap col-12 col-md-6 col-lg-4">
                <img src="../assets/photos/default_profile_pic/${player.icon_name}"
                alt="Retail Admin" class="recap_img">
                <div class="recap_wrap">
                    <div class="recap_player_name">${player.name}</div>
                    <div class="recap_text">${data.player_answer}</div>
                </div>
            </div>
            `
        )
    }

    game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;


}

// recap vote detail----------------
async function vote_recap_func(vote_record, dead_player, skipped_vote_record){
    // console.log(dead_player.player.player_role)

    // let res = await fetch("/recap_vote")
    // let result = await res.json();
    let recap_vote_list = vote_record;

    console.log("recap_data_list", recap_vote_list)
    let next_round = game_room.cur_game_round + 1

    game_chat_wrap.insertAdjacentHTML("beforeend", 
    /*html*/`
    <div class="chat_msg chat_left">
        <div class="chat_avatar">
            <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                alt="Retail Admin" class="chat_avatar_img">
        </div>

        <div class="chat_main ">
            <div class="player_name">Game Master Announcement</div>
            <div class="chat_text announcement">
                <div class="game_recap_header"><h3>Round ${game_room.cur_game_round} vote Result:</h3></div>
                <div class="game_recap_main_wrap">
                    <div class="voting_result_recap">
                    <span id="round_${game_room.cur_game_round}_vote_recap">
                    </span>

                        <br>
                            <span id="round_${game_room.cur_game_round}_vote_recap_brief">
                            </span>
                        <br>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
    )

    let round_vote_recap = document.querySelector(`#round_${game_room.cur_game_round}_vote_recap`)
    // recap_vote_list [{voter: "cat", votee: 'rabbit'}, {...}, {...}, ...]
    let vote_reform_list = [];
    if(recap_vote_list.length > 0){
        for(let vote_data of recap_vote_list){

            let data = {voter:[vote_data.voter] , vote_to: vote_data.vote_to}
            let repeat_vote = vote_reform_list.filter( item => item.vote_to == vote_data.vote_to)
            if(repeat_vote.length >0){
                repeat_vote[0].voter.push(vote_data.voter);
                let index = vote_reform_list.indexOf(repeat_vote[0]);
                if (index != -1) {
                    vote_reform_list[index] = repeat_vote[0];
                }
            }else{
                vote_reform_list.push(data)
            }
        }

        for(let vote_data of vote_reform_list){
            async function add_vote(){
                let player_vote_to = player_list.filter(player => player.id == vote_data.vote_to)

                round_vote_recap.insertAdjacentHTML("beforeend",
                /*html*/`
                    <div><span id="voter_of_${vote_data.vote_to}_round_${game_room.cur_game_round}"></span> voted ${player_vote_to[0].name}</div>
                `)
            }
            await add_vote();
            let voter_of_div = document.querySelector(`#voter_of_${vote_data.vote_to}_round_${game_room.cur_game_round}`)

            if(vote_data.voter.length == 1){
                let voter_obj = player_list.filter(player => player.id == vote_data.voter)
                // console.log(`${vote_data.voter}`)
                voter_of_div.insertAdjacentHTML("beforeend",`${voter_obj[0].name}`)
            }else{
                for(let voter of vote_data.voter){
                    let voter_obj = player_list.filter(player => player.id == voter)
                    voter_of_div.insertAdjacentHTML("beforeend",`${voter_obj[0].name}`)
                    if(vote_data.voter.indexOf(voter) < vote_data.voter.length -1){
                        voter_of_div.insertAdjacentHTML("beforeend",`, `)
                    }
                }
            }
        }
    }

    if(skipped_vote_record){
        round_vote_recap.insertAdjacentHTML("beforeend",
        /*html*/`
            <div><span id="voter_of_skippedVote_round_${game_room.cur_game_round}"></span> have skipped vote!</div>
        `)
        let voter_of_div = document.querySelector(`#voter_of_skippedVote_round_${game_room.cur_game_round}`)
        
        for(let skippedVote of skipped_vote_record){    
            let voter_obj = player_list.filter(player => player.id == skippedVote.voter)

            // console.log(voter_obj[0].name)
            voter_of_div.insertAdjacentHTML("beforeend",`${voter_obj[0].name}`)
            
            if(skipped_vote_record.indexOf(skippedVote) < skipped_vote_record.length -1 ){
                //   console.log(', ')
                voter_of_div.insertAdjacentHTML("beforeend",`, `)
            }
        }

    }

    



    //-----Calculation and define who is death?-----
    //--- re-write

    let round_vote_recap_brief = document.querySelector(`#round_${game_room.cur_game_round}_vote_recap_brief`)


    if(dead_player){
        // console.log(dead_player.player.player_role)
        let is_spy_string = (dead_player.player.player_role == "spy"? "Spy": "Not Spy")

        round_vote_recap_brief.insertAdjacentHTML("beforeend", 
        /*html*/`
            <h5>${dead_player.player.name} has been voted to death</h5>
            <h5>${dead_player.player.name} is <span class="fst-italic">${is_spy_string}</span></h5>
        `)
    }else{
        round_vote_recap_brief.insertAdjacentHTML("beforeend", 
        /*html*/`
            <h5>Nobody die this round!!</h5>
        `)
    }


    // game_chat_wrap.insertAdjacentHTML("beforeend", 
    // /*html*/`
    // <div class="chat_msg chat_left">
    //     <div class="chat_avatar">
    //         <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
    //             alt="Retail Admin" class="chat_avatar_img">
    //     </div>

    //     <div class="chat_main ">
    //         <div class="player_name">Game Master Announcement</div>
    //         <div class="chat_text announcement">
    //             <div class="vote_next_round_time_left font-monospace"><h3>Round ${next_round} (${game_room.game_phase} Phase) begin now! </h3></div>
    //         </div>
            
    //     </div>
    // </div>

    // `
    // )

    game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;
}

socket.on("next_round", async (content)=>{
    let next_round = content.next_round;
    let game_phase = content.game_phase;

    game_chat_wrap.insertAdjacentHTML("beforeend", 
    /*html*/`
    <div class="chat_msg chat_left">
        <div class="chat_avatar">
            <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                alt="Retail Admin" class="chat_avatar_img">
        </div>

        <div class="chat_main ">
            <div class="player_name">Game Master Announcement</div>
            <div class="chat_text announcement">
                <div class="vote_next_round_time_left font-monospace"><h3>Round ${next_round} (${game_phase} Phase) begin now! </h3></div>
            </div>
            
        </div>
    </div>
    `
    )
    game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;

})

socket.on("normal_player_win", async ()=>{
    console.log("normal player win")

    game_phase_main.innerHTML = "Ended (Normal Player Wins)"

    setTimeout(() => {
        alert("Normal Player Wins!!")

        game_chat_wrap.insertAdjacentHTML("beforeend", 
        /*html*/`
        <div class="chat_msg chat_left">
            <div class="chat_avatar">
                <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                    alt="Retail Admin" class="chat_avatar_img">
            </div>

            <div class="chat_main ">
                <div class="player_name">Game Master Announcement</div>
                <div class="chat_text announcement">
                    <div class="vote_next_round_time_left">
                        <!-- <h3> 1 (Gaming Phase) begin now!</h3> -->
                        <div><h3>Game Ended!!</h3></div>
                        <div><h3>Normal Player wins!!</h3></div>
                    </div>
                </div>
                
            </div>
        </div>
        `
        )
        game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;


    },500)

})

socket.on("spy_win", ()=>{
    console.log("Spy player win")

    game_phase_main.innerHTML = "Ended (Spy Player Wins)"

    setTimeout(() => {
        alert("spy_win!!!")
        game_chat_wrap.insertAdjacentHTML("beforeend", 
        /*html*/`
        <div class="chat_msg chat_left">
            <div class="chat_avatar">
                <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                    alt="Retail Admin" class="chat_avatar_img">
            </div>

            <div class="chat_main ">
                <div class="player_name">Game Master Announcement</div>
                <div class="chat_text announcement">
                    <div class="vote_next_round_time_left">
                        <div><h3>Game Ended!!</h3></div>
                        <div><h3>Spy wins!!</h3></div>
                    </div>
                </div>
                
            </div>
        </div>
        `
        )
        game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;

    },500)
})

socket.on("game_ended", ()=>{
    game_room.game_phase = "Ended";
    disable_vote_btn();
    enable_input();
    // console.log("game_phase :", game_room.game_phase)

    setTimeout(() => {
        alert("Game Ended!!")
        //Add to Game chat game ended

        game_chat_wrap.insertAdjacentHTML("beforeend",
        /*html*/`
        <div class="chat_msg chat_left">
            <div class="chat_avatar">
                <img src="./assets/photos/default_profile_pic/game-master.icon.jpg"
                    alt="Retail Admin" class="chat_avatar_img">
            </div>

            <div class="chat_main ">
                <div class="player_name">Game Master Announcement</div>
                <div class="chat_text announcement">
                    <div class="vote_next_round_time_left">
                        <div>Please choose where to go next!</div>
                        <button 
                            class="btn btn-info m-1"
                            onclick="(function(){window.location.href='/landing.html'  } )()"
                        >
                            Home Page
                        </button>
                        <button 
                            class="btn btn-warning m-1"
                        >
                            View Game Result
                        </button>
                    </div>
                </div>
                
            </div>
        </div>
        `
        )
        game_chat_wrap.scrollTop = game_chat_wrap.scrollHeight;
    },700)
})





// disabling vote_btn
function disable_vote_btn(){
    vote_btn.setAttribute("disabled", "");
}

function enable_vote_btn(){
    vote_btn.removeAttribute("disabled");
}



// disabling text boxes
function disable_input(){
    submitting_text_input.setAttribute("disabled","");
    text_submit_btn.setAttribute("disabled","");
}

async function enable_input(){
    submitting_text_input.removeAttribute("disabled");
    text_submit_btn.removeAttribute("disabled");
}



function new_text_box(player_name, submitting_text, text_direction, icon_name){
    game_chat_wrap.insertAdjacentHTML("beforeend", 
    /*html*/`
        <div class="chat_msg ${text_direction}">
            <div class="chat_avatar ">
                <img src="./assets/photos/default_profile_pic/${icon_name}"
                    alt="Retail Admin" class="chat_avatar_img">
            </div>

            <div class="chat_main">
                <div class="player_name">${player_name}</div>
                <div class="chat_text">
                    ${submitting_text}
                </div>
            </div>

        </div>
    `)
}






// init-------------------------------------------

async function get_player_list(){
    let res = await fetch("/player_list_with_room_id",{
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({"room_id": game_room.id})
    })
    let result = await res.json();

    player_list = result.data.return_list;
    return player_list;
}

async function load_votes(){
    let player_list = await get_player_list();

    for(let player of player_list){
        voting_main.insertAdjacentHTML("beforeend",  
            /*html*/`
            <div class="input_wrap col-6">
                <input type="radio" name="being_voted" class="vote_player_input" id="${player.id}" value="${player.name}" >
                <label class="voting_player" for="${player.id}">
                    <div class="player_recap">
                        <img src="../assets/photos/default_profile_pic/${player.icon_name}"
                        alt="Retail Admin" class="recap_img">
                        <div class="recap_wrap">
                            <div class="recap_player_name">${player.name}</div>
                        </div>
                    </div>
                </label>
            </div>
        `)
    }
    voting_main.insertAdjacentHTML('beforeend',
    /*html*/`
        <div class="input_wrap col-6">
            <input type="radio" name="being_voted" class="vote_player_input" id="skipVote_id" value="skipVote_name" >
            <label class="voting_player" for="skipVote_id">
                <div class="player_recap">
                    <img src="../assets/photos/functional_icons/stop_sign.png"
                    alt="Retail Admin" class="recap_img">
                    <div class="recap_wrap">
                        <div class="recap_player_name">Skip vote</div>
                    </div>
                </div>
            </label>
        </div>
    `
    )

}



async function update_data(){
        // update player in session
        let update_res = await fetch("/request_player", {
            method: "PUT"
        })
        let update_result = await update_res.json();
    
        // get the player object form session
        let user_res = await fetch("/request_player")
        let user_result = await user_res.json()
    
        let game_res = await fetch("/current_game_room")
        let game_result = await game_res.json();
    
        // --current_player, cur_game
        cur_player = user_result.data.player;
        game_room = game_result.data.game;
        console.log("is current_player dead: ",cur_player.is_dead)

        if(cur_player.is_dead == true && game_room.game_phase != "Ended"){
            disable_input();
            disable_vote_btn();
        }
}



async function init(){

    await update_data();


    game_id_value.innerHTML = cur_player.room_id;
    show_ans_answer.innerHTML= cur_player.player_role_answer;
    game_phase_value.innerHTML= game_room.game_phase;
    show_ans_player_identity.innerHTML = `(You are <span style="color:red">${cur_player.player_role}</span> player)`


    await load_votes();

    // console.log("12asd34")
    // console.log(player_list)


}

// alert("Abcd")



init();










