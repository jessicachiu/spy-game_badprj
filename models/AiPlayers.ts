import { get_random_int } from "../helpers/gen_random";
import { AiPlayer } from "./AiPlayer";




export class AiPlayers{

    constructor(private player:AiPlayer){
        this.player = player;
    }

    get_player(){
        return this.player;
    }

    get_player_id(){
        return this.player.id;
    }

    get_player_room_id(){
        return this.player.room_id;
    }
    get_is_ready(){
        return this.player.isReady;
    }

    get_player_role(){
        return this.player.player_role
    }
    get_player_role_answer(){
        return this.player.player_role_answer;
    }

    get_player_answer_list(){
        return this.player.player_answer_list;
    }
    get_player_round_answer(round:number){
        return this.player.player_answer_list[round]
    }

    get_vote_record_by_round(round:number){
        return this.player.vote_record_id_array[round];
    }

    get_is_dead(){
        return this.player.is_dead;
    }






    set_player_ready(){
        this.player.isReady = true;
    }

    set_player_unready(){
        this.player.isReady = false;
    }

    set_player_role(player_role:string){
        this.player.player_role = player_role
    }

    set_player_role_answer(answer:string){
        this.player.player_role_answer = answer
    }

    set_is_dead(is_dead:boolean){
        this.player.is_dead = is_dead
    }

    add_vote_record_id_array(player_id:string){
        this.player.vote_record_id_array.push(player_id)
    }


    add_player_answer_list(player_answer:string){
        this.player.player_answer_list.push(player_answer)
    }

    player_voted(){
        this.player.player_vote_count +=1
    }
    
    
    //ai functions here

    set_ai_describe_list(describe_list:string[]){
        this.player.ai_describe_list = describe_list;
    }

    get_ai_describe_list(){
        return this.player.ai_describe_list;
    }

    set_ai_answered_list(answer:string){
        this.player.ai_answered_list?.push(answer);
    }

    get_ai_answered_list(){
        return this.player.ai_answered_list;
    }

    get_set_ai_new_answer(){
        let avaliable_list = this.player.ai_describe_list.filter(item => !this.player.ai_answered_list?.includes(item))
        let avaliable_answer_number = avaliable_list.length;
        let random_number = get_random_int( 0, avaliable_answer_number);

        this.set_ai_answered_list(avaliable_list[random_number]);
        this.add_player_answer_list(avaliable_list[random_number]);
        

        return avaliable_list[random_number];
    }

}