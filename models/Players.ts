import { Player } from "./Player";

export class Players{

    constructor(private player:Player){
        this.player = player;
    }

    get_player(){
        return this.player;
    }

    get_player_id(){
        return this.player.id;
    }

    get_player_room_id(){
        return this.player.room_id;
    }
    get_is_ready(){
        return this.player.isReady;
    }

    get_player_role(){
        return this.player.player_role
    }
    get_player_role_answer(){
        return this.player.player_role_answer;
    }

    get_player_answer_list(){
        return this.player.player_answer_list;
    }
    get_player_round_answer(round:number){
        return this.player.player_answer_list[round]
    }

    get_vote_record_by_round(round:number){
        return this.player.vote_record_id_array[round];
    }

    get_is_dead(){
        return this.player.is_dead;
    }






    set_player_ready(){
        this.player.isReady = true;
    }

    set_player_unready(){
        this.player.isReady = false;
    }

    set_player_role(player_role:string){
        this.player.player_role = player_role
    }

    set_player_role_answer(answer:string){
        this.player.player_role_answer = answer
    }

    set_is_dead(is_dead:boolean){
        this.player.is_dead = is_dead
    }

    add_vote_record_id_array(player_id:string){
        this.player.vote_record_id_array.push(player_id)
    }


    add_player_answer_list(player_answer:string){
        this.player.player_answer_list.push(player_answer)
    }

    player_voted(){
        this.player.player_vote_count +=1
    }
}








