export interface AiPlayer{
    id: string,
    name: string
    user_id?: number,
    room_id: string,
    icon_name: string,
    player_vote_count: number,


    isReady ?: boolean,
    player_answer_list:string[],

    player_role_answer: string,
    player_role?:string,

    vote_record_id_array: string[],
    is_dead: boolean,

    ai_describe_list: string[],
    ai_answered_list: string[],

}