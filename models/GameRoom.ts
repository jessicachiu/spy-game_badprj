export interface GameRoom{
    id: string,
    player_id_list: string[],
    roundCount: number,
    nPlayerNum: number,
    spyNum: number,

    normalAns: string,
    spyAns: string,

    votingTime ?: number,
    gamingTime ?: number,
    winingRole ?: string,

    normal_player_id_list: string[],
    spyPlayer_id_list: string[],

    ready_player_number: number,
    game_phase: string,
    vote_count: number,


    cur_game_round: number,
    cur_answered_player_count: number,
    // player_des_list: player_describe_obj[]

    dead_spy_count: number,
    dead_normal_player_count :number,

    is_game_ended?:boolean,

    //ai_game_play
    ai_number: number,
    ai_id_list: string[],

}