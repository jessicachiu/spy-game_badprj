import { GameRoom } from "./GameRoom";

export class Game{
    // private game_room: GameRoom
    constructor(private game_room:GameRoom){
        // this.player = player_active
        this.game_room = game_room;
    }

    public get_game_room(){
        return this.game_room;
    }

    public get_game_room_id(){
        return this.game_room.id;
    }



    get_ready_player_number(){
        return this.game_room.ready_player_number + this.game_room.ai_number;
    }

    get_round_count(){
        return this.game_room.roundCount;
    }

    get_game_phase(){
        return this.game_room.game_phase;
    }

    
    get_normal_player_num(){
        return this.game_room.nPlayerNum;
    }
    get_spy_player_num(){
        return this.game_room.spyNum;
    }
    get_total_player_number_without_ai(){
        return this.game_room.nPlayerNum + this.game_room.spyNum - this.game_room.ai_number;
    }
    get_total_player_number(){
        return this.game_room.nPlayerNum + this.game_room.spyNum;
    }
    

    get_spy_player_list(){
        return this.game_room.spyPlayer_id_list;
    }
    get_normal_player_list(){
        return this.game_room.normal_player_id_list;
    }
    get_player_id_list(){
        return this.game_room.player_id_list;
    }

    get_player_id_list_without_ai(){
        if(this.game_room.ai_id_list){
            return this.game_room.player_id_list.filter(item => !this.game_room.ai_id_list.includes(item))
        }else{
            return this.game_room.player_id_list
        }
    }

    get_ai_id_list(){
        return this.game_room.ai_id_list;
    }


    get_spy_answer(){
        return this.game_room.spyAns;
    }
    get_normal_answer(){
        return this.game_room.normalAns;
    }


    get_cur_game_round(){
        return this.game_room.cur_game_round;
    }
    get_cur_answered_player_count(){
        return this.game_room.cur_answered_player_count;
    }

    get_vote_count(){
        return this.game_room.vote_count;
    }
    
    get_dead_spy_count(){
        return this.game_room.dead_spy_count;
    }
    get_dead_normal_player_count(){
        return this.game_room.dead_normal_player_count;
    }
    get_survive_player_number(){
        let undeadplayer = this.get_total_player_number() - (this.get_dead_spy_count() + this.get_dead_normal_player_count());
        return undeadplayer;
    }




    set_game_phase_gaming(){
        this.game_room.game_phase = 'Gaming';
    }
    
    set_game_phase_voting(){
        this.game_room.game_phase = 'Voting';
    }

    set_game_phase_end(){
        this.game_room.game_phase = 'Ended';
    }


    
    add_player_id_list(player_id:string){
        this.game_room.player_id_list.push(player_id);
    }

    add_normal_player_id_list(player_id:string){
        this.game_room.normal_player_id_list.push(player_id)
    }

    add_spy_player_id_list(player_id:string){
        this.game_room.spyPlayer_id_list.push(player_id)
    }

    add_ai_id_list(ai_id:string){
        this.game_room.ai_id_list.push(ai_id)
    }

    add_dead_spy_count(){
        this.game_room.dead_spy_count +=1;
    }
    add_dead_normal_player_count(){
        this.game_room.dead_normal_player_count +=1;
    }


    set_normal_answer(answer:string){
        this.game_room.normalAns = answer;
    }
    set_spy_answer(answer:string){
        this.game_room.spyAns = answer;
    }







    add_cur_answered_player_count(){
        this.game_room.cur_answered_player_count +=1;
    }

    add_vote_count(){
        this.game_room.vote_count +=1;
    }



    player_ready(){
        this.game_room.ready_player_number += 1;
    }


    reset_vote_count(){
        this.game_room.vote_count = 0;
    }

    reset_cur_answered_player_count(){
        this.game_room.cur_answered_player_count = 0;
    }


    round_end(){
        this.game_room.cur_game_round += 1;
    }

    is_game_ended(){
        if( this.game_room.cur_game_round > this.game_room.roundCount ){
            return true;
        }else if(this.game_room.is_game_ended == true){
            return true;
        }else{
            return false;
        }
    }

    is_spy_win(){
        if(this.game_room.roundCount < this.game_room.cur_game_round){
            this.game_end('Spy');
            return true;
        }else if(this.game_room.dead_normal_player_count >= (this.game_room.nPlayerNum)) {
            this.game_end('Spy');
            return true;
        }else{
            return false;
        }
    }

    is_normal_player_win(){
        if(this.game_room.dead_spy_count >= (this.game_room.spyNum)){
            this.game_end('Normal');
            return true;
        }else{
            return false;
        }
    }
    

    game_end(winingRole:string){
        this.game_room.cur_game_round = this.game_room.roundCount;
        this.game_room.winingRole = winingRole;
        this.set_game_phase_end();
        this.game_room.is_game_ended = true;
    }



    //ai-game-play
    get_ai_number(){
        return this.game_room.ai_number;
    }




}



