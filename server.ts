
import express from 'express';
import {Request,Response} from 'express';
import path from "path";
import http from 'http';
import { Server as SocketIO} from 'socket.io';
import { client } from "./db";
import env, { checkEnv } from './env';
import { createRoutes } from './routes/routes';
import { setSocketIO } from './helpers/socketIO-Util';
import { sessionMiddleware } from './helpers/session';

// ---------controllers----------------------------

const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);
console.log('check env now');

checkEnv()

//-------------------Middle Ware----------------------------------------------
app.use(express.json());
app.use(sessionMiddleware);

io.use((socket,next)=>{
    let req = socket.request as express.Request
    let res = req.res as express.Response
    sessionMiddleware(req, res, next as express.NextFunction)
});

// -----------------socket

setSocketIO(io)

createRoutes({
    app,
    client
})

app.get('/',function(req:Request,res:Response){
    // req.session["counter"]++;
    // console.log("counter: ",req.session["counter"], req.method, req.path)
    res.sendFile(path.resolve("public","landing.html"));
})
app.use(express.static("public"))

//------------------------------------404 page-----------------------------------

//Error Page if all page not available(get method)
app.use((req: Request, res: Response) => {
    res.status(404).sendFile(path.resolve("./public/404.html"));
});

//------------------------------------404 page-----------------------------------



//----------------------------Port and start-UP---------------------------
const PORT = env.PORT;

server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});

//----------------------------Port and start-UP---------------------------

