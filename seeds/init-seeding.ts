import { Knex } from "knex";
import {hashPassword} from '../helpers/hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("game_msg").del();
    await knex("vote").del();
    await knex("game_room").del();
    await knex("game_topic").del();
    await knex("users").del();




    // Inserts seed entries
    await knex("users").insert([
        { email: "Admin@admin.com", password: await hashPassword('admin') , username: "Admin" },
        { email: "111@111.com", password: await hashPassword('11111') , username: "User_test_1" },
        { email: "222@222.com", password: await hashPassword('22222') , username: "User_test_2" },
        { email: "333@333.com", password: await hashPassword('33333') , username: "User_test_3" },
        { email: "444@444.com", password: await hashPassword('44444') , username: "User_test_4" },
        { email: "555@555.com", password: await hashPassword('55555') , username: "User_test_5" },
        { email: "noName@noNmae.com", password: await hashPassword('noName')},
    ]);




    await knex("game_topic").insert([
        { normal_ans: "spiderman", spy_ans: "deadpool" },
        { normal_ans: "captain_american", spy_ans: "winter_solider"},
        { normal_ans: "superman", spy_ans: "batman"},
        { normal_ans: "wolverine", spy_ans: "X-23"},
        { normal_ans: "dr_strange", spy_ans: "hulk"},
        { normal_ans: "thor", spy_ans: "loki"},
        { normal_ans: "captain_marvel", spy_ans: "scarlet_witch"},
        { normal_ans: "ultron", spy_ans: "vision"},
        { normal_ans: "ironman", spy_ans: "blackpanther"},
        { normal_ans: "flash", spy_ans: "quick_silver"},
    ]);



};
